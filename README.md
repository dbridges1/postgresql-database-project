# Postgres Project

## Setup Instructions
1. Using Git Bash, Clone the repository
2. Open pgAdmin and create a new database called **postgres_project**
3. Restore the dump file **project_dump.backup**


## Relational Databases
Name all foreign key relationships in this database, including the table
name of the original table, the foreign key field name, and the table that
that foreign key points to. One foreign key relationship has been provided
in the template README, please follow this format.

### Foreign Key Relationships

| Table Name  | Foreign Key (FK) Name  | Table Matching FK |
| --- | --- | --- |
| purchases | customer_id | customers |
| purchase_items | item_id | items |
| inventory | item_id | items |
| customers | store_id | stores |
| purchases | store_id | stores |
| inventory | store_id | stores |
| stores | address_id | address |
| purchase_items | purchase_id | purchases |
|purchases | payment_type_id | payment_types |



### Multiple Choice

1. Users are not allowed to add purchases where the amount of items bought exceeds the amount of items in the store. This is because: 
    1. There is a Trigger Function preventing new purchases that exceed the current inventory.

2. Why will the following SQL statement not work in the stores database?
   >SELECT s.name AS "Store Name", a.street AS "Street Address", a.street2 AS "Suite", a.city AS "City", a.state AS "State", a.zip AS "Zip Code"
   >FROM stores s
   >INNER JOIN address a
   >ON address_id = a.id
    4. This is a valid query that will run without error.
    
3. What is the relationship between the stores and customers tables?
    2. Stores is a one-to-many relationship with Customers.

4. What is the relationship between the purchases and items tables? 

    1. The tables have a many-to-many relationship with the purchase_items table acting as a mapping table.